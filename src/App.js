import './App.css';
import {Container} from "@mui/material";
import ListMovies from "./pages/ListMovies";
import {useDispatch} from "react-redux";
import {init} from "./utils";

function App() {
    const dispatch = useDispatch();
    dispatch(init());

      return (
          <Container maxWidth="lg">
              <ListMovies />
          </Container>
      );
}

export default App;
