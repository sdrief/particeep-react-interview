import {SET_MOVIES} from "./action-types";

export const setMovies = data => ({
    type: SET_MOVIES,
    data
});
