import React, {useState} from 'react';
import {
    Box,
    Card,
    CardActions,
    CardContent,
    CardHeader,
    Chip,
    FormControl,
    Grid,
    IconButton, InputLabel, MenuItem, OutlinedInput, Pagination, Select,
    Stack, TablePagination
} from "@mui/material";
import FavoriteIcon from '@mui/icons-material/Favorite';
import DeleteIcon from '@mui/icons-material/Delete';
import {useDispatch, useSelector} from "react-redux";
import ThumbUpIcon from '@mui/icons-material/ThumbUp';
import ThumbDownIcon from '@mui/icons-material/ThumbDown';
import {setMovies} from "../actions";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};

const ListMovies = () => {
    const movies = useSelector(state => state.movies);
    const dispatch = useDispatch();
    const [categories, setCategories] = useState([]);
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(4);

    const deleteMovie = (movie) => {
        const newList = movies.filter(item => (item.id !== movie.id));
        setCategories(getCategories(newList).filter(category => categories.includes(category)));
        dispatch(setMovies(newList));
        const visibleMovies = newList.filter(element => element.isVisible).length;
        const lastPage = Math.ceil(visibleMovies / rowsPerPage) - 1; // - 1 because index start à 0
        if (lastPage >= 0 && lastPage < page) {
            setPage(lastPage);
        }
    };

    const toggleFavorite = (key) => {
        movies[key].isFavorite = !movies[key].isFavorite;
        dispatch(setMovies([...movies]));
    }

    const handleChange = (event) => {
        const listCategories = event.target.value;
        setCategories(listCategories);
        const moviesByCategories = movies.map(item => {
            item.isVisible = listCategories.length === 0 ? true : listCategories.includes(item.category);
            return item;
        });
        dispatch(setMovies([...moviesByCategories]));
        setPage(0);
    }

    const getCategories = (list = []) => {
        const allCategories = (list.length ? list : movies).map(item => item.category);
        return [...new Set(allCategories)];
    }

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    return (
        <>
            <Grid container>
                <FormControl sx={{m: 1, width: 300}}>
                    <InputLabel id="demo-multiple-chip-label">Catégorie</InputLabel>
                    <Select
                        labelId="categoryLabelId"
                        id="categoryId"
                        multiple
                        value={categories}
                        onChange={handleChange}
                        input={<OutlinedInput id="select-multiple-chip" label="Chip"/>}
                        renderValue={(selected) => (
                            <Box sx={{display: 'flex', flexWrap: 'wrap', gap: 0.5}}>
                                {selected.map((value) => (
                                    <Chip key={value} label={value}/>
                                ))}
                            </Box>
                        )}
                        MenuProps={MenuProps}
                    >
                        {getCategories().map((val, key) => (
                            <MenuItem
                                key={key}
                                value={val}
                            >
                                {val}
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>
            </Grid>
            <Grid container rowSpacing={2} columnSpacing={{xs: 1, sm: 2, md: 3}}>
                {movies.filter(element => element.isVisible)
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((item, key) => (
                    <Grid item xs={12} sm={6} md={3} key={item.id}>
                        <Card>
                            <CardHeader
                                title={item.title}
                                subheader={item.category}
                            />
                            <CardContent>
                                <Stack direction="row" spacing={2}>
                                    <Chip icon={<ThumbUpIcon/>} label={item.likes}/>
                                    <Chip icon={<ThumbDownIcon/>} label={item.dislikes}/>
                                </Stack>
                            </CardContent>
                            <CardActions disableSpacing>
                                <IconButton aria-label="add to favorites" onClick={() => toggleFavorite(key)}
                                            color={item.isFavorite ? "primary" : "default"}>
                                    <FavoriteIcon/>
                                </IconButton>

                                <IconButton aria-label="delete" onClick={() => deleteMovie(item)}>
                                    <DeleteIcon/>
                                </IconButton>
                            </CardActions>
                        </Card>
                    </Grid>
                ))}
            </Grid>
            <Grid container>
                <TablePagination
                    component="div"
                    count={movies.filter(element => element.isVisible).length}
                    page={page}
                    onPageChange={handleChangePage}
                    rowsPerPage={rowsPerPage}
                    rowsPerPageOptions={[4, 8, 12]}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                />
            </Grid>
        </>
    );
};

export default ListMovies;
