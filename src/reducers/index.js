import {combineReducers} from 'redux'
import {moviesReducer} from "./movies";

const appReducer = combineReducers({
    movies: moviesReducer,
});

export const rootReducer = (state, action) => {
    return appReducer(state, action);
};

