import {SET_MOVIES} from "../actions/action-types";

export const moviesReducer = (state = [], action) => {
    if (action.type === SET_MOVIES) {
        return action.data;
    }

    return state;
};
