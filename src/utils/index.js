import {movies$} from "../api/movies";
import {setMovies} from "../actions";

const filterByCategory = (data) => {
    let newData = {};
    data.forEach(item => {
        if (!newData[item.category]) {
            newData[item.category] = [];
        }
        newData[item.category].push(item);
    });
    return newData;
}

export const init = () => {
    return (dispatch, getState) => {
        movies$.then(response => {
            if (response) {
                //const movies = sortByCategory(response);

                dispatch(setMovies(response.map(v => ({...v, isFavorite: false, isVisible: true}))));
            }
        })
    }
}

export const getMovies = () => {

}